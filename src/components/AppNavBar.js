import {useContext} from 'react';
import {Nav, Navbar, Image} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import userContext from '../userContext';
import BrandName from '../images/official-name.png';

export default function AppNavBar() {

	const {user} = useContext(userContext);

	return (
		<>
		<Navbar expand="md" className="solid-background p-3 font-pt" sticky="top">
			<Navbar.Brand><Image src={BrandName} className='w-25 span'></Image></Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="justify-content-end">
					<Nav.Item> 
						<Nav.Link as={NavLink} exact to="/home" id='item-link'>Products</Nav.Link>
					</Nav.Item>
					{
						(user.token !== null) ? 
						<>
							<Nav.Item> 
							<Nav.Link as={NavLink} exact to="/" id='item-link'>Cart</Nav.Link> 
							</Nav.Item>
							<Nav.Item> 
							<Nav.Link as={NavLink} exact to="/logout" id='item-link'>Logout</Nav.Link> 
							</Nav.Item>
						</> : 
						<>
							<Nav.Item>
							<Nav.Link as={NavLink} exact to="/login" id='item-link'>Login</Nav.Link>
							</Nav.Item>
							<Nav.Item>
							<Nav.Link as={NavLink} exact to="/register" id='item-link'>Register</Nav.Link>
							</Nav.Item>
						</>
					}
					
				</Nav>
			</Navbar.Collapse> 
		</Navbar>
		</>
	)
}