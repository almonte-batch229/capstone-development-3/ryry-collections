import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col, Modal, Form} from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';
import AppNavBar from '../components/AppNavBar';
import swal from 'sweetalert2';
import userContext from '../userContext';
import Logo from '../images/official-logo-3.png';

export default function CourseView() {

	const {user} = useContext(userContext);
	const {productId} = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [isActive, setIsActive] = useState("");
	const [quantity, setQuantity] = useState("");
	const [is_Active, setIs_Active] = useState("");
	const [update, setUpdate] = useState(false);
	const [checkout, setCheckout] = useState(false);

  	const updateClose = () => setUpdate(false);
  	const updateShow = () => setUpdate(true);

  	const checkoutClose = () => setCheckout(false);
  	const checkoutShow = () => setCheckout(true);
  	
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setIsActive(data.isActive);

		});
	}, [productId])

	useEffect(() => {

		if(name !== '' && description !== '' && price !== '') {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [name, description, price]);

	useEffect(() => {

		if(quantity !== '') {
			setIs_Active(true)
		}
		else {
			setIs_Active(false)
		}
	}, [quantity]);

	function updateProduct(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			navigate(`/product/${productId}`);
		})
	}

	function archiveProduct(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			navigate(`/product/${productId}`);
		})
	}

	function retrieveProduct(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/retrieve/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			navigate(`/product/${productId}`);
		})
	}

	function checkOut(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {

				swal.fire({
					title: 'CHECK OUT SUCCESSFUL',
					icon: 'success',
					text: `${quantity} ${name} is added to your cart`,
					color: '#D3A625',
					background: '#0A0A0A',
					showConfirmButton: false,
  					timer: 2000
				});

				navigate(`/home`);
			}
			else {
				swal.fire({
					title: 'CHECK OUT FAILED',
					icon: 'error',
					text: 'There may be a system error. Please try again.',
					color: '#D3A625',
					background: '#0A0A0A',
					showConfirmButton: false,
  					timer: 2000
				})
			}
		})
	}

	return(
		<>
			<AppNavBar/>
			<Container className="mt-5">
				<Row>
					<Col lg={{ span: 6, offset: 3 }}>
						<Card>
							<Card.Body className="text-center">
								<Card.Img variant="top" src={Logo} className='w-50' />
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>Php {price}</Card.Text>
								{
									(user.token !== null) ? 
									<>
									{
										(user.isAdmin === true) ?
										<>
											<Button className='submitBtn mx-1' onClick={updateShow} block>Update Details</Button>
											{
												(isActive) ? 
												<>
												 	<Button className='submitBtn mx-1' onClick={e => archiveProduct(e)} block>Archive</Button>
												</>
												: <Button className='submitBtn mx-1' onClick={e => retrieveProduct(e)} block>Retrieve</Button>

											}
										</>
										:
										<>
											<Button className='submitBtn' onClick={checkoutShow} block>Check Out</Button>									</>
									}
									</>
									: <Button as={Link} to ="/login" className='submitBtn' block>LOGIN</Button>
								}
							</Card.Body>		
						</Card>
					</Col>
				</Row>
			</Container>

			<Modal show={update} onHide={updateClose} backdrop="static" keyboard={false}>
		      	<Form onSubmit={e => updateProduct(e)}>
			        <Modal.Header closeButton>
			          <Modal.Title>Edit Product Details</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			          
					    <Form.Group className="mb-3" controlId="name">
					        <Form.Label>Product Name</Form.Label>
					        <Form.Control type="text" placeholder="enter product name" value={name} onChange={e => setName(e.target.value)} required/>
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="description">
					        <Form.Label>Product Description</Form.Label>
					        <Form.Control type="textarea" placeholder="enter product description" value={description} onChange={e => setDescription(e.target.value)}required/>
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="price">
					        <Form.Label>Product Price</Form.Label>
					        <Form.Control type="number" placeholder="enter price" value={price} onChange={e => setPrice(e.target.value)} required/>
					    </Form.Group>    
			        </Modal.Body>
			        <Modal.Footer>
			          	<Button className="cancelBtn" onClick={updateClose}>Cancel</Button>
			          	{
							isActive ? <Button type="submit" id="confirmBtn" onClick={updateClose}>Update</Button> 
							: <Button type="submit" id="confirmBtn" disabled>Update</Button>
						}
			        </Modal.Footer>
		        </Form>
		    </Modal>

		    <Modal show={checkout} onHide={checkoutShow} backdrop="static" keyboard={false}>
		      	<Form onSubmit={e => checkOut(e)}>
			        <Modal.Header closeButton>
			          <Modal.Title>Check Out</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>	      
					    <Form.Group className="mb-3" controlId="quantity">
					        <Form.Label>Quantity</Form.Label>
					        <Form.Control type="number" placeholder="enter quantity" value={quantity} onChange={e => setQuantity(e.target.value)} required/>
					      </Form.Group>  
			        </Modal.Body>
			        <Modal.Footer>
			          	<Button className="cancelBtn" onClick={checkoutClose}>Cancel</Button>
			          	{
							is_Active ? <Button type="submit" className="confirmBtn" onClick={checkoutClose}>Check Out</Button> 
							: <Button type="submit" className="confirmBtn" disabled>CheckOut</Button>
						}
			        </Modal.Footer>
		        </Form>
		    </Modal>
		</>
	);
};
