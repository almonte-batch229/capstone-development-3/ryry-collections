import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Logo from '../images/official-logo-3.png';

export default function ProductList({productProps}) {

	const {name, price, _id} = productProps;

	return (
        <Card className="w-75 mx-auto mb-5 mt-2">
	        <Card.Body className="m-3">
	        	<Card.Img variant="top" src={Logo} className='w-25' />
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>Php {price}</Card.Text>
	        	<Button as={Link} to={`/product/${_id}`} className="submitBtn">View Details</Button>
	        </Card.Body>
	    </Card>
	)
}