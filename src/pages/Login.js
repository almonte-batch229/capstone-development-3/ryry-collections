import {useState, useEffect, useContext} from 'react';
import {Container, Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import AppNavBar from '../components/AppNavBar';
import swal from 'sweetalert2';
import userContext from '../userContext'; 

export default function Login() {

	const {user, setUser} = useContext(userContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {

		if(email !== '' && password !== '') {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [email, password]);

	function loginUser(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			if(typeof data.access_token !== 'undefined') {

				localStorage.setItem('token', data.access_token);

				retrieveUserDetails(data.access_token);
			}
			else {

				swal.fire({
					title: 'AUTHENTICATION FAILED',
					icon: 'error',
					text: 'check your login details and please try again...',
					color: '#D3A625',
					background: '#0A0A0A',
					showConfirmButton: false,
  					timer: 2000
				})
			}

		})

		setEmail('');
		setPassword('');
	}

	const retrieveUserDetails = (token) => {

    	fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    		headers: {
    			Authorization: `Bearer ${token}`
    		}
    	})
    	.then(res => res.json())
    	.then(data => {

    		setUser({
    			token: localStorage.getItem('token'),
    			id: data._id,
    			isAdmin: data.isAdmin,
    		})
    	})
	}

	return(	

		(user.token === null) ? 
		<>
			<AppNavBar/>
			<div class='black-background min-vh-100'>
				<Container className='container-box font-roboto lemon-curry px-5 pt-4'>
					<h2 className='text-center mb-3'>MEMBER LOGIN</h2>
					<Form onSubmit={e => loginUser(e)}>

					    <Form.Group className="mb-3" controlId="email">
					        <Form.Label>Email Address</Form.Label>
					        <Form.Control type="email" placeholder="enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="password">
					        <Form.Label>Password</Form.Label>
					        <Form.Control type="password" placeholder="enter password" value={password} onChange={e => setPassword(e.target.value)} required/>
					    </Form.Group>

					    <div className='text-center'>
					    {
	    					isActive ? <Button type="submit" className="submitBtn">LOGIN</Button> 
	    					: <Button type="submit" className="submitBtn" disabled>LOGIN</Button>
	    				}
	    				</div>
				    </Form>
				</Container>
			</div>
		</> 
		: <Navigate to="/home"/>
	);	
};