import {Container, Row, Col, Image, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Logo from '../images/official-logo-3.png';

export default function Home() {
	return (
		<>
			<div className='black-background min-vh-100'>
				<Container fluid className='font-roboto pt-5'>
					<Row className="justify-content-center">
						<Col>
							<Image src={Logo} className='mx-auto d-block w-50'></Image>
							<Button as={Link} to={'/home'} className='browse-box'>S T A R T &nbsp; B R O W S I N G</Button>
						</Col>
					</Row>
				</Container>
			</div>
		</>
	);
}; 