import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Image, Button, Modal, Form} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';
import AppNavBar from '../components/AppNavBar';
import userContext from '../userContext'; 
import ProductList from '../components/ProductList';
import cover from '../images/official-banner.png';

export default function Home() {

	const {user} = useContext(userContext);

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false)

	const [products, setProducts] = useState([]);
	const [activeProducts, setActiveProducts] = useState([]);
	const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

	useEffect(() => {

		fetch(`http://localhost:4000/products/all`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'  
			}
		})
    .then(res => res.json())
    .then(data => {

      setProducts(data.map(product => {
          return (
						<>
							<ProductList key={product._id} productProps={product}/>
						</>
					)
      }))
    })
	}, [])

	useEffect(() => {
		fetch(`http://localhost:4000/products/active`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		})
    .then(res => res.json())
    .then(data => {

      setActiveProducts(data.map(activeProduct => {
        return (
					<>
						<ProductList key={activeProduct._id} productProps={activeProduct}/>
					</>
				)
      }))
    })
	}, [])

	useEffect(() => {

		if(name !== '' && description !== '' && price !== '') {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
		
	}, [name, description, price]);

	function createProduct(e) {

		e.preventDefault();

		fetch('http://localhost:4000/products/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {

				navigate('/home');
			}
		})

		setName('');
		setDescription('');
		setPrice('');
	}

	return (
		<>
			<AppNavBar/>
			{
				(user.isAdmin === true) ?
					<Container className='text-center'>
						<h1 className='text-center mt-5'>Product Inventory</h1>
						<Button onClick={handleShow} className="submitBtn">ADD PRODUCT</Button>
						<Row className='px-5'>
							<Col className="px-5 py-3 text-center">
								{products}
							</Col>
						</Row>
					</Container>
				: 
				<Container>
						<Row>
							<Col className="p-5 text-center">
								<Image src={cover}></Image>
							</Col>
						</Row>
						<h1 className='text-center'>C O L L E C T I O N</h1>
						<Row className='px-5'>
							<Col className="px-5 py-3 text-center">
								{activeProducts}
							</Col>
						</Row>
					</Container>
			}
			
		  <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
	      <Form onSubmit={e => createProduct(e)}>
	        <Modal.Header closeButton>
	          <Modal.Title>Add a Product</Modal.Title>
	        </Modal.Header>
	        <Modal.Body>
			      <Form.Group className="mb-3" controlId="name">
			        <Form.Label>Product Name</Form.Label>
			        <Form.Control type="text" placeholder="enter product name" value={name} onChange={e => setName(e.target.value)} required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="description">
			        <Form.Label>Product Description</Form.Label>
			        <Form.Control type="textarea" placeholder="enter product description here" value={description} onChange={e => setDescription(e.target.value)}required/>
			      </Form.Group>

				    <Form.Group className="mb-3" controlId="price">
			        <Form.Label>Product Price</Form.Label>
			        <Form.Control type="number" placeholder="enter price" value={price} onChange={e => setPrice(e.target.value)} required/>
			      </Form.Group>
	        </Modal.Body>
	        <Modal.Footer>
	          <Button className="cancelBtn" onClick={handleClose}>Cancel</Button>
	          {
							isActive ? 
							<Button type="submit" className="confirmBtn" onClick={handleClose}>CREATE</Button> 
							: <Button type="submit" className="confirmBtn" disabled>CREATE</Button>
						}
	        </Modal.Footer>
	      </Form>
	    </Modal>
		</>
	);
}; 