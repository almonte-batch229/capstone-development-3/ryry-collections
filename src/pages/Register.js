import {useState, useEffect, useContext} from 'react';
import {Container, Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import AppNavBar from '../components/AppNavBar';
import swal from 'sweetalert2';
import userContext from '../userContext'; 

export default function Register() {

	const navigate = useNavigate();

	const {user} = useContext(userContext);
  
	const [username, setUsername] = useState('');
	const [email, setEmail] = useState('');
	const [mobile, setMobileNumber] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if(username !== '' && email !== '' && password !== '' && mobile.length === 11) {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [username, email, mobile, password]);

	function registerUser(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				username : username,
				email: email,
				contactNo: mobile,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {

				swal.fire({
					title: 'REGISTRATION SUCCESSFUL',
					icon: 'success',
					text: 'Redireting you to the login page now...',
					color: '#D3A625',
					background: '#0A0A0A',
					showConfirmButton: false,
  					timer: 2000
				});

				navigate('/login');
			}
			else {
				swal.fire({
					title: 'REGISTRATION FAILED',
					icon: 'error',
					text: 'Email already exists. Please use a different one.',
					color: '#D3A625',
					background: '#0A0A0A',
					showConfirmButton: false,
  					timer: 2000
				})
			}
		})

		setUsername('');
		setEmail('');
		setMobileNumber('');
		setPassword('');
	}

	return (

		(user.token === null) ? 
		<>
			<AppNavBar/>
			<div className='black-background min-vh-100'>
				<Container className='container-box font-roboto lemon-curry px-5 py-4'>
					<h2 className='text-center mb-3'>SIGN UP</h2>
					<Form onSubmit={e => registerUser(e)}>

						<Form.Group className="mb-3" controlId="email">
					        <Form.Label>Email Address</Form.Label>
					        <Form.Control type="email" placeholder="enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="username">
					        <Form.Label>Username</Form.Label>
					        <Form.Control type="text" placeholder="enter username" value={username} onChange={e => setUsername(e.target.value)}required/>
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="mobile">
					        <Form.Label>Mobile Number</Form.Label>
					        <Form.Control type="text" placeholder="enter mobile number" value={mobile} onChange={e => setMobileNumber(e.target.value)}required/>
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="password">
					        <Form.Label>Password</Form.Label>
					        <Form.Control type="password" placeholder="enter password" value={password} onChange={e => setPassword(e.target.value)}required/>
					    </Form.Group>

					    <div className='text-center'>
						{
	    					isActive ? <Button type="submit" className="submitBtn">REGISTER</Button> 
	    					: <Button type="submit" className="submitBtn" disabled>REGISTER</Button>
	    				}
	    				</div>
				    </Form>
				</Container>
			</div>
		</>
		: <Navigate to="/home"/>
	);	
};